package com.lebedeva.service;

import com.lebedeva.entity.Clan;
import com.lebedeva.exception.ClanNotFoundException;
import com.lebedeva.utility.FileLoggerOrConsoleOnException;

import java.util.logging.Logger;

public class TaskService {
    private final ClanService clanService;
    private static final Logger LOGGER = Logger.getLogger(TaskService.class.getName());

    private static final int TASK_COMPLETE_REWARD = 10;

    /**
     * @param clanService
     */
    public TaskService(ClanService clanService) {
        this.clanService = clanService;
        FileLoggerOrConsoleOnException.configure(LOGGER);
    }

    /**
     * Изменение прогресса по клановому заданию
     *
     * @param clanId ID клана, в котором выполняется клановое задание
     * @param taskId ID кланового задания
     * @param userId ID пользователя, совершившего прогресс по клановому заданию
     * @param value  количественная метрика прогресса по клановому заданию
     */
    public void progressTask(long clanId, long taskId, long userId, double value) {
        // .... saveProgressTrackerByUser
        double progress = 100.;
        if (progress == 100.) {
            completeTask(clanId, taskId);
        }
        // ...
    }

    /**
     * Добавление золота в казну клана при выполнении кланового задания
     *
     * @param clanId ID клана, в котором было выполнено задание
     * @param taskId ID кланового задания
     */
    private void completeTask(long clanId, long taskId) {
        // ...
        clanService.lockClan(clanId);
        try {
            Clan clan = clanService.get(clanId);
            clan.setGold(clan.getGold() + TaskService.TASK_COMPLETE_REWARD);
            if (clanService.save(clan, "\"taskId\": " + taskId)) {
                LOGGER.info("[SUCCESS] Добавлено золото (" + TaskService.TASK_COMPLETE_REWARD + ") в клан (" + clanId + ") за задание " + taskId);
            } else {
                LOGGER.severe("[FAIL] Ошибка при добавлении золота (" + TaskService.TASK_COMPLETE_REWARD + ") в клан (" + clanId + ") за задание " + taskId);
            }
        } catch (ClanNotFoundException e) {
            LOGGER.severe("[FAIL] Клан (" + clanId + ") не найден в таблице `clan`");
        } finally {
            clanService.unlockClan(clanId);
        }
        // ...
    }
}
