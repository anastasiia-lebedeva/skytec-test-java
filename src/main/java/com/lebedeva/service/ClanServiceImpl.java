package com.lebedeva.service;

import com.lebedeva.entity.Clan;
import com.lebedeva.exception.ClanNotFoundException;
import com.lebedeva.repository.ClanRepository;
import com.lebedeva.repository.TrackerClanRepository;
import com.lebedeva.utility.FileLoggerOrConsoleOnException;

import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class ClanServiceImpl implements ClanService {
    /**
     * Сервис для работы с информацией о клане
     */
    private final ClanRepository clanRepository;
    /**
     * Сервис для трекинга изменения в клане
     */
    private final TrackerClanRepository trackerClanRepository;
    /**
     * Инструмент для логирования
     */
    private static final Logger LOGGER = Logger.getLogger(ClanServiceImpl.class.getName());

    /**
     * Мапа для получения локов по каждому клану отдельно
     */
    private final ConcurrentMap<Long, Lock> clanLocks = new ConcurrentHashMap<>();

    public ClanServiceImpl() {
        FileLoggerOrConsoleOnException.configure(LOGGER);

        try {
            this.clanRepository = new ClanRepository();
            this.trackerClanRepository = new TrackerClanRepository();
        } catch (SQLException e) {
            LOGGER.severe("[FAIL] Не удалось подключиться к БД");
            throw new RuntimeException(e);
        }
    }

    /**
     * Получение сущности клана по ID
     *
     * @param clanId ID клана, для которого нужно получить сущность
     * @return сущность клана, если он был найден в БД по clanId
     * @throws ClanNotFoundException исключение об отсутствии клана в БД, если нет соответствующей записи с clanId
     */
    @Override
    public Clan get(long clanId) throws ClanNotFoundException {
        return clanRepository.getClanById(clanId).orElseThrow(() -> new ClanNotFoundException(clanId));
    }

    /**
     * Сохранение нового стейтемента клана с указанием источника воздействия
     *
     * @param clan   Клан, в котором произошли изменения
     * @param source Источник изменений
     * @return true, если успешно сохранены данные в clanRepository и trackerClanRepository
     */
    @Override
    public boolean save(Clan clan, String source) {

        Optional<Clan> oldClan = clanRepository.getClanById(clan.getId());
        int deltaGold = clan.getGold() - (oldClan.map(Clan::getGold).orElse(0));
        return clanRepository.saveClan(clan) && trackerClanRepository.saveGoldDelta(clan, deltaGold, source);

    }

    /**
     * Захват лока для клана clanId
     *
     * @param clanId ID клана
     */
    @Override
    public void lockClan(long clanId) {
        LOGGER.info("[PROCESS " + Thread.currentThread().getName() + "] Захват лока для клана " + clanId);
        Lock lock = clanLocks.computeIfAbsent(clanId, k -> new ReentrantLock());
        lock.lock();
        LOGGER.info("[SUCCESS " + Thread.currentThread().getName() + "] Lock захвачен для клана " + clanId + ", lock: " + lock);
    }

    /**
     * Освобождение лока для клана clanId
     *
     * @param clanId ID клана
     */
    @Override
    public void unlockClan(long clanId) {
        LOGGER.info("[PROCESS " + Thread.currentThread().getName() + "]  Освобождение лока для клана " + clanId);
        Lock lock = clanLocks.get(clanId);
        LOGGER.info("[PROCESS " + Thread.currentThread().getName() + "]  Для клана " + clanId + " найден lock: " + lock);
        if (lock == null) {
            LOGGER.severe("[ERROR] Нельзя разблокировать lock для клана " + clanId + ", lock == null.\n" +
                    "Возможно, метод unlockClan() был вызван раньше, чем lockClan()");
            throw new NullPointerException("У клана " + clanId + "не создан lock");
        }
        lock.unlock();
        LOGGER.info("[SUCCESS " + Thread.currentThread().getName() + "]  Освобожден лок для клана " + clanId);
    }

}
