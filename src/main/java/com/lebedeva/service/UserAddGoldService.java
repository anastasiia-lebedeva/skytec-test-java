package com.lebedeva.service;

import com.lebedeva.entity.Clan;
import com.lebedeva.exception.ClanNotFoundException;
import com.lebedeva.utility.FileLoggerOrConsoleOnException;

import java.util.logging.Logger;

/**
 * Пример сервиса, с помощью которого пользователь добавляет золото в казну клана
 */
public class UserAddGoldService {
    /**
     * Сервис для работы с кланом, через который сохраняются данные в БД
     */
    private final ClanService clanService;

    /**
     * Инструмент для логирования событий в этом классе
     */
    private static final Logger LOGGER = Logger.getLogger(UserAddGoldService.class.getName());

    /**
     * @param clanService Реализация сервиса для работы с кланом
     */
    public UserAddGoldService(ClanService clanService) {
        this.clanService = clanService;
        FileLoggerOrConsoleOnException.configure(LOGGER);
    }

    /**
     * @param userId    ID пользователя, добавившего золото в казну
     * @param clanId    ID клана, в казну которого добавляется золото
     * @param addedGold количество золота
     */
    public void addGoldToClan(long userId, long clanId, int addedGold) {
        clanService.lockClan(clanId);
        try {
            Clan clan = clanService.get(clanId);
            clan.setGold(clan.getGold() + addedGold);
            if (clanService.save(clan, "\"userId\": " + userId)) {
                LOGGER.info("[SUCCESS] Добавлено золото (" + addedGold + ") в клан (" + clanId + ") пользователем " + userId);
            } else {
                LOGGER.severe("[FAIL] Ошибка при добавлении золота (" + addedGold + ") в клан (" + clanId + ") пользователем " + userId);
                // returnGoldToUser(userId, addedGold);
            }
        } catch (ClanNotFoundException e) {
            LOGGER.severe("[FAIL] Клан (" + clanId + ") не найден в таблице `clan`");
            // returnGoldToUser(userId, addedGold);
        } finally {
            clanService.unlockClan(clanId);
        }
    }

}
