package com.lebedeva.service;

import com.lebedeva.entity.Clan;
import com.lebedeva.exception.ClanNotFoundException;

public interface ClanService {
    public Clan get(long clanId) throws ClanNotFoundException;

    public boolean save(Clan clan, String source);

    void lockClan(long clanId);

    void unlockClan(long clanId);
}
