package com.lebedeva.utility;

import java.io.IOException;
import java.util.logging.*;

/**
 * Инструмент базового логирования в файл со стандартным форматированием
 * Если файл не удалось открыть для логирования, то используется стандартный консольный логер
 */
public final class FileLoggerOrConsoleOnException {
    /**
     * Хендлер для вывода логов, вынесен в статическую переменную, чтобы все классы писались в один файл
     */
    private static final Handler handler = initHandler();

    /**
     * Определение типа хендлера для логирования и установка SimpleFormatter для форматирования хендлера
     *
     * @return FileHandler, если удалось открыть файл для логирования. Иначе ConsoleHandler
     */
    private static Handler initHandler() {
        Handler handler = null;
        try {
            handler = new FileHandler("application.log");
        } catch (IOException e) {
            System.err.println("Невозможно открыть файл для логирования");
            handler = new ConsoleHandler();
        } finally {
            if (handler != null) {
                handler.setFormatter(new SimpleFormatter());
            }
        }
        return handler;
    }

    /**
     * Ассоциирование внутриклассового логера с общим хендлером
     */
    public static void configure(Logger logger) {
        logger.addHandler(handler);
    }
}
