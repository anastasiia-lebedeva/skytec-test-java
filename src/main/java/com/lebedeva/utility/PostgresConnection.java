package com.lebedeva.utility;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Получению URL для подключения по JDBC к БД
 */
public final class PostgresConnection {
    public static final String connectionUrl = init();

    static String init() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            HashMap<String, String> map = objectMapper.readValue(new File("./src/main/resources/database.json")
                    , new TypeReference<HashMap<String, String>>() {
                    });
            return "jdbc:" + map.get("driver") + "://" + map.get("server") + ":" + map.get("port") + "/" +
                    map.get("database") + "?user=" + map.get("user") + "&password=" + map.get("password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private PostgresConnection() {
    }

}
