package com.lebedeva.exception;

/**
 * Ошибка, если не удалось найти клан по clanId
 */
public class ClanNotFoundException extends Exception {
    public ClanNotFoundException(long clanId) {
        super("Не найден клан " + clanId);
    }
}
