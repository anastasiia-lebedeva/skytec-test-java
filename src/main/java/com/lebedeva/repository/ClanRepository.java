package com.lebedeva.repository;

import com.lebedeva.entity.Clan;
import com.lebedeva.utility.FileLoggerOrConsoleOnException;
import com.lebedeva.utility.PostgresConnection;

import java.sql.*;
import java.util.Optional;
import java.util.logging.Logger;


/**
 * Информация о клане в БД
 */
public class ClanRepository implements AutoCloseable {

    /**
     * Подключение к БД
     */
    private final Connection connection;
    /**
     * Инструмент логирования. По умолчанию создается с FileHandler, если возникли проблемы с файлом, то используется
     * ConsoleHandler
     */
    private static final Logger LOGGER = Logger.getLogger(ClanRepository.class.getName());

    public ClanRepository() throws SQLException {
        this.connection = DriverManager.getConnection(PostgresConnection.connectionUrl);
        connection.setAutoCommit(false);

        FileLoggerOrConsoleOnException.configure(LOGGER);
    }

    /**
     * По clanId получаем entity.Clan или null
     *
     * @param clanId ID клана, для которого создаем сущность
     * @return Clan с информацией, найденной по clanId, или null, если не найден клан с таким clanId
     * @see com.lebedeva.exception.ClanNotFoundException ClanNotFoundException
     */
    public Optional<Clan> getClanById(long clanId) {
        String query = "SELECT id, name, gold " +
                "FROM clan " +
                "WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, clanId);
            ResultSet rs = statement.executeQuery();
            return Optional.ofNullable(rs.next()
                    ? new Clan(rs.getLong("id"), rs.getString("name"), rs.getInt("gold"))
                    : null);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Сохранение измененной информации по клану
     * @param clan Клан, в котором произошли изменения
     * @return true, если новое состояние клана было успешно сохранено, иначе false
     */
    public boolean saveClan(Clan clan) {
        String query = "INSERT INTO clan (id, name, gold) VALUES (?, ?, ?) " +
                "ON CONFLICT ON CONSTRAINT clan_pk_id DO " +
                "UPDATE SET name = ?, gold = ? WHERE clan.id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, clan.getId());
            statement.setString(2, clan.getName());
            statement.setInt(3, clan.getGold());
            statement.setString(4, clan.getName());
            statement.setInt(5, clan.getGold());
            statement.setLong(6, clan.getId());

            int countAffectedRows = statement.executeUpdate();
            if (countAffectedRows > 1) {
                connection.rollback();
                throw new SQLException("Не может быть изменено больше одного клана");
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void close() throws Exception {
        this.connection.close();
    }
}
