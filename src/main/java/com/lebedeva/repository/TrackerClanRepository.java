package com.lebedeva.repository;

import com.lebedeva.entity.Clan;
import com.lebedeva.utility.FileLoggerOrConsoleOnException;
import com.lebedeva.utility.PostgresConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Трекер об изменении информации о клане в БД
 */
public class TrackerClanRepository implements AutoCloseable {
    /**
     * Подключение к БД
     */
    private final Connection connection;
    /**
     * Инструмент логирования. По умолчанию создается с FileHandler, если возникли проблемы с файлом, то используется
     * ConsoleHandler
     */
    private static final Logger LOGGER = Logger.getLogger(TrackerClanRepository.class.getName());

    public TrackerClanRepository() throws SQLException {
        this.connection = DriverManager.getConnection(PostgresConnection.connectionUrl);
        connection.setAutoCommit(false);

        FileLoggerOrConsoleOnException.configure(LOGGER);
    }


    /**
     * Сохранение изменения в состоянии казны клана
     *
     * @param clan      клан, в котором произошли изменения
     * @param deltaGold число, на которое увеличилось/убавилось золота в казне
     * @param source    источник, из-за которого произошла транзакция
     * @return true, если данные были успешно сохранены, иначе false
     */
    public boolean saveGoldDelta(Clan clan, int deltaGold, String source) {
        String query = "INSERT INTO tracker_clan_coffers (clan_id, delta_gold, source) VALUES (?, ?, (?)::json)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, clan.getId());
            statement.setInt(2, deltaGold);
            statement.setString(3, "{" + source + "}");

            int countAffectedRows = statement.executeUpdate();
            if (countAffectedRows > 1) {
                connection.rollback();
                throw new SQLException("Не может быть создано несколько строк трекинга для одного события");
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}
