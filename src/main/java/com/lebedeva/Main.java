package com.lebedeva;

import com.lebedeva.service.ClanService;
import com.lebedeva.service.ClanServiceImpl;
import com.lebedeva.service.TaskService;
import com.lebedeva.service.UserAddGoldService;

public class Main {
    public static void main(String[] args) {
        ClanService clanService = new ClanServiceImpl();
        UserAddGoldService userAddGoldService = new UserAddGoldService(clanService);
        TaskService taskService = new TaskService(clanService);

        new Thread(() -> {
            userAddGoldService.addGoldToClan(1L, 2L, 100);
            userAddGoldService.addGoldToClan(3L, 2L, 10);
        }).start();
        userAddGoldService.addGoldToClan(2L, 2L, 1);
        taskService.progressTask(1, 1, 1, 100.);
    }
}
