package com.lebedeva.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Сущность клана, в которой хранится:
 * <ul>
 *     <li>id - уникальный ID клана</li>
 *     <li>name - название клана</li>
 *     <li>gold - текущее количество золота в казне клана</li>
 * </ul>
 */
@Data
@AllArgsConstructor
public class Clan {
    /**
     * ID клана
     */
    private long id;
    /**
     * Название клана
     */
    private String name;
    /**
     * Количество золота в казне клана
     */
    private int gold = 0;

    /** Создается клан с указанным id и name, казна пустая (gold = 0)
     * @param id ID клана
     * @param name название клана
     */
    public Clan(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
